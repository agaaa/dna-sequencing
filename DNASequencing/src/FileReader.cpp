#include "FileReader.h"

using namespace std;

FileReader::FileReader(int _wordLength) {
    this->wordLength = _wordLength;
}


// ------------------------------------------------------------------------------- //


FileReader::~FileReader() {

}


// ------------------------------------------------------------------------------- //


// creating a vector of files in directory
void FileReader::GetFilesInDirectory(string _dirName, vector<char*> & _filesNames) {
    DIR *dir;
    struct dirent *ent;

    if ((dir = opendir (_dirName.c_str())) == NULL) {
        cout << "Could not open the directory \"" << _dirName << "\"." << endl;
        cout << "Errno: " << errno << " - ";
        switch(errno) {
            case EACCES:
                cout << "EACCES: Permission denied." << endl;
                break;
            case EBADF:
                cout << "EBADF: fd is not a valid file descriptor opened for reading." << endl;
                break;

            case EMFILE:
                cout << "EMFILE: Too many file descriptors in use by process." << endl;
                break;

            case ENFILE:
                cout << "ENFILE: Too many files are currently open in the system." << endl;
                break;

            case ENOENT:
                cout << "ENOENT: Directory does not exist, or name is an empty string." << endl;
                break;

            case ENOMEM:
                cout << "ENOMEM: Insufficient memory to complete the operation." << endl;
                break;

            case ENOTDIR:
                cout << "ENOTDIR: name is not a directory." << endl;
                break;
        }
    }   
    else {
        while ((ent = readdir (dir)) != NULL) {
            if (ent->d_name[0] != '.')
                _filesNames.push_back(ent->d_name);
        }
        closedir (dir);
    }   
}


// ------------------------------------------------------------------------------- //


void FileReader::ReadData(string _path, vector<string> & _data) {

    ifstream fileIn;
    fileIn.open(_path, ifstream::in);
    if(!fileIn.good())
        cout << "Error while opening file \"" << _path << "\"." << endl;
    else {
        string word;
        while(getline(fileIn, word)) {
            _data.push_back(word);
        }
        fileIn.close();     
    }
}



