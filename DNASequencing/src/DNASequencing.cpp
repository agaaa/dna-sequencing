#include "DNASequencing.h"

using namespace std;

DNASequencing::DNASequencing(string _dirName, unsigned int _n) 
    : n(_n) {

    this->dirName = _dirName;
    this->fileReader = new FileReader(10);

    //getting a list of files in given directory
    this->fileReader->GetFilesInDirectory(this->dirName, this->filesNames);

}
 

// ----------------------------------------------------------- //


DNASequencing::~DNASequencing() {
    (this->filesNames).clear();
    delete fileReader;
}


// ----------------------------------------------------------- //


void DNASequencing::Run() {

    stringstream ss;
    string dataFileName, path;

    //result file
    string resultFileName;
    ss << this->n;
    ss >> resultFileName;
    ss.clear();
    resultFileName = "../../results/result" + resultFileName;

    //delete the content
    ofstream ofs;
    ofs.open(resultFileName);
    ofs.close();

    // for each instance (file in directory)
    for (unsigned int i=0; i<(this->filesNames).size(); ++i) {

        ss << (this->filesNames).at(i);
        ss >> dataFileName;
        ss.clear();
        path = this->dirName + "/" + dataFileName;
        cout << "Processing " << path <<  "..." << endl;

        // reading data
        vector<string> data;
        (this->fileReader)->ReadData(path, data);
        
        Algorithm *algorithm = new Algorithm(this->n, data, dataFileName, resultFileName);
        algorithm->Solve();

        data.clear();
    }
}   
