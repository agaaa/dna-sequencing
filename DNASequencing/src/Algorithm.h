#ifndef ALGORITHM
#define ALGORITHM

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string>
#include <time.h>

#include "Def.h"
#include "HungarianAlgorithm.h"

class Algorithm {

    private:
        HungarianAlgorithm *hungarianAlgorithm;
        Matrix *matrix;
        Graph *graph;
        Def *def;

        unsigned int n;
        unsigned int *choosenZeros;
        std::vector<std::pair<unsigned int, unsigned int>> distances; //index, value
        std::vector<std::string> oligs;

        unsigned int positive;
        unsigned int negative;

        std::string dataFileName;
        std::string resultFileName;

        clock_t start;
        clock_t stop;

    public:
        Algorithm(unsigned int _n, std::vector<std::string> data, std::string _dataFileName, std::string _resultFileName);
        ~Algorithm();

        void Solve();
        
        bool CheckIfSolved(unsigned int &_num, unsigned int &_sumOfTheCycle);
        unsigned int GetFalseIndex(bool *_tab);
        
        unsigned int FindSolution(unsigned int _first, unsigned int &_sum);
        void FillVector(unsigned int _num);
        unsigned int FindTheWorst(unsigned int _num, unsigned int _elem, unsigned int &_first);
        unsigned int DeleteTheWorst(unsigned int &_num, unsigned int _elem);
        void PrintSolution(unsigned int _num);
        unsigned int FirstIndex(unsigned int _first);

        void ImproveSolution();
        void RemoveCycles();

        void PrintResults(unsigned int _first, unsigned int _length);
};

#endif
