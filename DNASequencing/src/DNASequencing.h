#ifndef DNA
#define DNA

#include "FileReader.h"
#include "Matrix.h"
#include "HungarianAlgorithm.h"
#include "Algorithm.h"
#include "Def.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdio.h>
#include <string.h>

class DNASequencing {

    private:
        FileReader* fileReader;
        std::vector<char*> filesNames;
        std::string dirName;
        const unsigned int n;

    public:
        DNASequencing(std::string _dirName, unsigned int _n);
        ~DNASequencing();

        void Run();

};

#endif
