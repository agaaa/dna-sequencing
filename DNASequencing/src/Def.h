#ifndef DEF
#define DEF

#define INF 4294967295
#define OLIG_LENGTH 10

#include <iostream>
#include <vector>

class Def {

    private:

    public:
        Def();
        ~Def();

        bool Contains(unsigned int _value, std::vector<unsigned int> _vec);
        bool Remove(unsigned int _value, std::vector<unsigned int> &_vec);

};

#endif
