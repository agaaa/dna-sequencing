#ifndef FILE_READER
#define FILE_READER

#include <iostream>
#include <fstream>
#include <vector>
#include <dirent.h>
#include <sys/types.h>

#include "Def.h"

class FileReader {

    private:
        int wordLength;

    public:
        FileReader(int _wordLength);
        ~FileReader();

        void GetFilesInDirectory(std::string _dirName, std::vector<char*> & _filesNames);
        void ReadData(std::string _fileName, std::vector<std::string> & _data);
};

#endif
