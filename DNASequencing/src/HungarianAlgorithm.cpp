#include "HungarianAlgorithm.h"

using namespace std;

HungarianAlgorithm::HungarianAlgorithm(Matrix *_matrix) {
    this->matrix = _matrix;
    this->def = this->matrix->GetDef();
    this->InitVariables();
}


// ----------------------------------------------------------- //


HungarianAlgorithm::~HungarianAlgorithm() {
    for(unsigned int i=0; i<this->matrix->GetOligsNum(); ++i) {

        //nienaznaczone zera
        this->zerosInRows[i].clear();
        this->zerosInCols[i].clear();

        //naznaczone zera
        this->choosenZerosInRows[i] = INF;
        this->choosenZerosInCols[i] = INF;

        //usunięte zera
        this->deletedZerosInRows[i].clear();
        this->deletedZerosInCols[i].clear();
    }

    //oznaczone wiersze i kolumny
    this->choosenRows.clear();
    this->choosenCols.clear();

    //pokryte liniami wiersze i kolumny
    this->linesRows.clear();
    this->linesCols.clear();

}


// ----------------------------------------------------------- //


void HungarianAlgorithm::Solve() {

    this->SubtractMinFromRows();
    this->SubtractMinFromCols();

    this->FillVariables();
    this->MainLoop();

    /*for(unsigned int i=0; i<this->matrix->GetOligsNum(); ++i)
        cout << i << " -> " << this->choosenZerosInRows[i] << endl;
    cout << endl;*/
}


// ----------------------------------------------------------- //


//(a)
void HungarianAlgorithm::SubtractMinFromRows() {

    unsigned int min;
    for(unsigned int i=0; i<this->matrix->GetOligsNum(); ++i) {
        min = this->matrix->MinInRow(i);
        if(min != 0)
            this->matrix->SubtractFromRow(i, min);
    }
}


// ----------------------------------------------------------- //


//(b)
void HungarianAlgorithm::SubtractMinFromCols() {
    unsigned int min;
    for(unsigned int i=0; i<this->matrix->GetOligsNum(); ++i) {
        min = this->matrix->MinInCol(i);
        if(min != 0)
            this->matrix->SubtractFromCol(i, min);
    }
}


// ----------------------------------------------------------- //


void HungarianAlgorithm::InitVariables() {

    //nienaznaczone zera
    this->zerosInRows = new vector<unsigned int>[this->matrix->GetOligsNum()];
    this->zerosInCols = new vector<unsigned int>[this->matrix->GetOligsNum()];

    //naznaczone zera
    this->choosenZerosInRows = new unsigned int[this->matrix->GetOligsNum()];
    this->choosenZerosInCols = new unsigned int[this->matrix->GetOligsNum()];

    //usunięte zera
    this->deletedZerosInRows = new vector<unsigned int>[this->matrix->GetOligsNum()];
    this->deletedZerosInCols = new vector<unsigned int>[this->matrix->GetOligsNum()];
}


// ----------------------------------------------------------- //


void HungarianAlgorithm::FillVariables() {
    for(unsigned int i=0; i<this->matrix->GetOligsNum(); ++i) {

        //getting zeros in each row and column
        this->matrix->GetZerosInRow(i, this->zerosInRows[i]);
        this->matrix->GetZerosInCol(i, this->zerosInCols[i]);

        //initial values
        choosenZerosInRows[i] = INF;
        choosenZerosInCols[i] = INF;
    }
}


// ----------------------------------------------------------- //


void HungarianAlgorithm::UpdateVariables() {
    for(unsigned int i=0; i<this->matrix->GetOligsNum(); ++i) {
        for(unsigned int j=0; j<this->matrix->GetOligsNum(); ++j) {
            //getting zeros in each row and column
            if(this->matrix->GetElement(i,j) == 0
                    && this->def->Contains(j, this->zerosInRows[i]) == false
                    && this->def->Contains(j, this->deletedZerosInRows[i]) == false
                    && this->choosenZerosInRows[i] != j) {
                this->zerosInRows[i].push_back(j);
                this->zerosInCols[j].push_back(i);
            }
        }
    }
}


// ----------------------------------------------------------- //


void HungarianAlgorithm::ClearVariables() {

    for(unsigned int i=0; i<this->matrix->GetOligsNum(); ++i) {

        //nienaznaczone zera
        this->zerosInRows[i].clear();
        this->zerosInCols[i].clear();

        //naznaczone zera
        this->choosenZerosInRows[i] = INF;
        this->choosenZerosInCols[i] = INF;

        //usunięte zera
        this->deletedZerosInRows[i].clear();
        this->deletedZerosInCols[i].clear();
    }

    //oznaczone wiersze i kolumny
    this->choosenRows.clear();
    this->choosenCols.clear();

    //pokryte liniami wiersze i kolumny
    this->linesRows.clear();
    this->linesCols.clear();
}


// ----------------------------------------------------------- //


void HungarianAlgorithm::MainLoop() {

    bool solved = false, check = false;
    bool modified;

    //(e)
    do {
        modified = false;

        this->CheckTheOnlyOneZerosInRows(modified); //(c)
        this->CheckTheOnlyOneZerosInCols(modified); //(d)

        if (modified == false) {
            if(this->ZerosInRowsAllocated() == true) //(f) - go to first option
                solved = true;
            else if(this->UncheckedZerosExist() == true) //(f) - go to second option
                this->CheckNWZero(); 
            else 
                check = true; //go to (g)
        }
    } while (solved == false && check == false);

    if(check == true)
        this->CheckingRowsAndCols();
}


// ----------------------------------------------------------- //


//Jeżeli w wierszu znajduje się dokładnie jedno nie naznaczone 0 - naznacz je.
//Skreśl inne zera w tej kolumnie aby nie powtórzyć przydziału wykonawcy
void HungarianAlgorithm::CheckTheOnlyOneZerosInRows(bool &modified) {
    unsigned int colNum;

    for(unsigned int i=0; i<this->matrix->GetOligsNum(); ++i) {

        if(this->zerosInRows[i].size() == 1) {

            colNum = zerosInRows[i].front();

            //selecting the zero in row if it is the only one
            this->choosenZerosInRows[i] = colNum; //in row number i element in the vector
            this->choosenZerosInCols[colNum] = i;

            //deleting checked zero and other zeros in this column
            for(unsigned int j=0; j<this->zerosInCols[colNum].size(); ++j) //dla każdego 0 w kolumnie
                if (this->zerosInCols[colNum].at(j) != i) { //jeżeli to nie naznaczone zero
                    this->deletedZerosInCols[colNum].push_back(this->zerosInCols[colNum].at(j));
                    this->deletedZerosInRows[this->zerosInCols[colNum].at(j)].push_back(colNum);
                }

            this->ClearFromRows(colNum);
            this->zerosInCols[colNum].clear();
            this->zerosInRows[i].clear();

            modified = true;
        }
    }
}


// ----------------------------------------------------------- //


//W kolumnie zawierającej dokładnie jedno nie naznaczone 0, naznacz je symbolem 0.
//Skreśl pozostałe zera w odpowiednim wierszu.
void HungarianAlgorithm::CheckTheOnlyOneZerosInCols(bool &modified) {

    unsigned int rowNum;

    for(unsigned int i=0; i<this->matrix->GetOligsNum(); ++i) {
        if(this->zerosInCols[i].size() == 1) {

            rowNum = zerosInCols[i].front();

            //selecting the zero in column if it is the only one
            this->choosenZerosInCols[i] = rowNum; //in row number i element in the vector
            this->choosenZerosInRows[rowNum] = i;

            //deleting checked zero and other zeros in this row
            for(unsigned int j=0; j<this->zerosInRows[rowNum].size(); ++j)
                if (this->zerosInRows[rowNum].at(j) != i){
                    this->deletedZerosInRows[rowNum].push_back(this->zerosInRows[rowNum].at(j));
                    this->deletedZerosInCols[this->zerosInRows[rowNum].at(j)].push_back(rowNum);
                }

            this->ClearFromCols(rowNum);
            this->zerosInRows[rowNum].clear();
            this->zerosInCols[i].clear();

            modified = true;
        }
    }
}


// ----------------------------------------------------------- //


void HungarianAlgorithm::CheckNWZero() {

    bool found = false;
    unsigned int rowNum = 0, colNum;

    while((found == false) && (rowNum<this->matrix->GetOligsNum())) {
        if(this->zerosInRows[rowNum].size() > 0) {

            colNum = zerosInRows[rowNum].front();

            //selecting the zero in row if it is the only one
            this->choosenZerosInRows[rowNum] = colNum; //in row number i element in the vector
            this->choosenZerosInCols[colNum] = rowNum;

            //deleting checked zero and other zeros in this row
            for(unsigned int j=1; j<this->zerosInRows[rowNum].size(); ++j) {
                this->deletedZerosInRows[rowNum].push_back(this->zerosInRows[rowNum].at(j));
                this->deletedZerosInCols[this->zerosInRows[rowNum].at(j)].push_back(rowNum);
            }

            //deleting checked zero and other zeros in this column
            for(unsigned int j=1; j<this->zerosInCols[colNum].size(); ++j) {
                this->deletedZerosInCols[colNum].push_back(this->zerosInCols[colNum].at(j));
                this->deletedZerosInRows[this->zerosInCols[colNum].at(j)].push_back(colNum);
            }

            //deleting checked zero and other zeros in this column
            this->ClearFromRows(colNum);
            this->ClearFromCols(rowNum);
            this->zerosInCols[colNum].clear();
            this->zerosInRows[rowNum].clear();

            found = true;
        }
        ++rowNum;
    }
}


// ----------------------------------------------------------- //


void HungarianAlgorithm::ClearFromRows(unsigned int _colNum) {

    unsigned int i = 0;
    while(i<this->matrix->GetOligsNum()) {
        this->def->Remove(_colNum, this->zerosInRows[i]);
        ++i;
    }
}


// ----------------------------------------------------------- //


void HungarianAlgorithm::ClearFromCols(unsigned int _rowNum) {
    unsigned int i = 0;
    while(i<this->matrix->GetOligsNum()) {
        this->def->Remove(_rowNum, this->zerosInCols[i]);
        ++i;
    }
}


// ----------------------------------------------------------- //


bool HungarianAlgorithm::ZerosInRowsAllocated() {
    for(unsigned int i=0; i<this->matrix->GetOligsNum(); ++i)
        if(this->choosenZerosInRows[i] == INF)
            return false;
    return true; //każdy wierwsz ma przydział 0
}


// ----------------------------------------------------------- //


bool HungarianAlgorithm::UncheckedZerosExist() {
    for(unsigned int i=0; i<this->matrix->GetOligsNum(); ++i)
        if(this->zerosInRows[i].empty() == false)
            return true; //nienaznaczone zera istnieją
    return false; //nienaznaczonych zer nie ma
}



// ----------------------------------------------------------- //


//go to (g), (j)
void HungarianAlgorithm::CheckingRowsAndCols() {

    this->choosenRows.clear();
    this->choosenCols.clear();  

    this->CheckRowsWithoutCheckedZeros();

    bool modified;
    do {
        modified = false;
        this->CheckColWithZero(modified);
        this->CheckRowWithCheckedZerosInCheckCols(modified);
    } while(modified == true);

    this->CoverRowsAndCols();
}


// ----------------------------------------------------------- //


//(g)
void HungarianAlgorithm::CheckRowsWithoutCheckedZeros() {
    for(unsigned int rowNum=0; rowNum<this->matrix->GetOligsNum(); ++rowNum)
        if(this->choosenZerosInRows[rowNum] == INF) {
            this->choosenRows.push_back(rowNum);
        }
}


// ----------------------------------------------------------- //


//(h)
void HungarianAlgorithm::CheckColWithZero(bool &modified) {
    unsigned int row;
    for(unsigned int i=0; i<this->choosenRows.size(); ++i) {
        row = this->choosenRows.at(i);
        for(unsigned int j=0; j<this->matrix->GetOligsNum(); ++j) { //this->choosenRows.at(i) - numer wiersza
            if(this->matrix->GetElement(row, j) == 0) {
                if(this->def->Contains(j, this->choosenCols) == false) {
                    this->choosenCols.push_back(j);
                    modified = true;
                }
            }
        }
    }
}


// ----------------------------------------------------------- //


//(i)
void HungarianAlgorithm::CheckRowWithCheckedZerosInCheckCols(bool &modified) {

    unsigned int col;
    for(unsigned int i=0; i<this->choosenCols.size(); ++i) {
        col = this->choosenCols.at(i);
        for(unsigned int j=0; j<this->matrix->GetOligsNum(); ++j) {
            if(this->choosenZerosInRows[j] == col) {
                if(this->def->Contains(j, this->choosenRows) == false) {
                    this->choosenRows.push_back(j);
                    modified = true;
                }
            }
        }
    }
}


// ----------------------------------------------------------- //


void HungarianAlgorithm::CoverRowsAndCols() {

    this->linesRows.clear();
    this->linesCols.clear();

    //go to (k)
    this->CoverRows();
    this->CoverCols();

    //(l)
    unsigned int choosen = this->GetCheckedZerosNumber();
    if(((this->linesRows.size() + this->linesCols.size()) == choosen )
            && (choosen != this->matrix->GetOligsNum())) {
        unsigned int min = this->MinUnlinedElement();
        this->matrix->SubtractFromUnlinedRowsAndAddToLinedCols(min, this->choosenRows, this->choosenCols);
        this->ClearVariables();
        this->FillVariables();
        this->MainLoop();
    }
    else { //go to (m)
        this->SolveGraph();
    }
}


// ----------------------------------------------------------- //


//(k)
void HungarianAlgorithm::CoverRows() {
    for(unsigned int rowNum=0; rowNum<this->matrix->GetOligsNum(); ++rowNum) {
        if(this->def->Contains(rowNum, this->choosenRows) == false) {
            this->linesRows.push_back(rowNum);
        }
    }
}


// ----------------------------------------------------------- //


//(k)
void HungarianAlgorithm::CoverCols() {
    for(unsigned int colNum=0; colNum<this->matrix->GetOligsNum(); ++colNum) {
        if(this->def->Contains(colNum, this->choosenCols) == true) {
            this->linesCols.push_back(colNum);
        }
    }
}


// ----------------------------------------------------------- //


unsigned int HungarianAlgorithm::GetCheckedZerosNumber() {
    unsigned int choosen = 0;
    for(unsigned int rowNum=0; rowNum<this->matrix->GetOligsNum(); ++rowNum) {
        if(this->choosenZerosInRows[rowNum] != INF)
            ++choosen;
    }
    return choosen;
}


// ----------------------------------------------------------- //


unsigned int HungarianAlgorithm::MinUnlinedElement() {
    unsigned int min = INF;
    for(unsigned int rowNum=0; rowNum<this->matrix->GetOligsNum(); ++rowNum) {
        if(this->def->Contains(rowNum, this->linesRows) == false)
            for(unsigned int colNum=0; colNum<this->matrix->GetOligsNum(); ++colNum) {
                if(this->def->Contains(colNum, this->linesCols) == false) {
                    if(this->matrix->GetElement(rowNum, colNum) < min)
                        min = this->matrix->GetElement(rowNum, colNum);
                }
            }
    }
    return min;
}


// ----------------------------------------------------------- //


//(m)
void HungarianAlgorithm::SolveGraph() {

    this->graph = new Graph(this->matrix->GetOligsNum(), this->def);

    vector<unsigned int> path;
    unsigned int i, pathSize, rowNum, colNum;
    this->graph->Clear(); 
    this->graph->Create(this->choosenZerosInRows, this->choosenZerosInCols, this->deletedZerosInRows);
    if(this->graph->FindPath(path) == false) 
        cout << "Path not found!" << endl; 

    i=1;
    rowNum = path.at(0);
    pathSize = path.size();
    while(i < pathSize) {
        colNum = path.at(i) - this->matrix->GetOligsNum();
        ++i;

        //naznacz rowNum, colNum
        this->choosenZerosInRows[rowNum] = colNum;
        this->choosenZerosInCols[colNum] = rowNum;
        this->def->Remove(colNum, this->deletedZerosInRows[rowNum]);
        this->def->Remove(rowNum, this->deletedZerosInCols[colNum]);

        rowNum = path.at(i);
        ++i;
        if(rowNum != 2*this->matrix->GetOligsNum()+1) {
            this->choosenZerosInRows[rowNum] = INF;
            this->choosenZerosInCols[colNum] = INF;
            this->deletedZerosInRows[rowNum].push_back(colNum);
            this->deletedZerosInCols[colNum].push_back(rowNum);
        }
    }
    path.clear(); 
    if(this->ZerosInRowsAllocated() == false) {
        this->CheckingRowsAndCols();
    }

}


// ----------------------------------------------------------- //


void HungarianAlgorithm::PrintZerosInRows() {

    cout << "Zeros in rows" << endl;
    for (unsigned int i=0; i<this->matrix->GetOligsNum(); ++i) {
        cout << i << ": ";
        for(unsigned int j=0; j<this->zerosInRows[i].size(); ++j)
            cout << this->zerosInRows[i].at(j) << " ";
        cout << endl;
    }
}


// ----------------------------------------------------------- //


void HungarianAlgorithm::PrintZerosInCols() {
    cout << "Zeros in columns" << endl;
    for (unsigned int i=0; i<this->matrix->GetOligsNum(); ++i) {
        cout << i << ": ";
        for(unsigned int j=0; j<this->zerosInCols[i].size(); ++j)
            cout << this->zerosInCols[i].at(j) << " ";
        cout << endl;
    }
}


// ----------------------------------------------------------- //


void HungarianAlgorithm::PrintDeletedZerosInRows() {

    cout << "Deleted zeros in rows" << endl;
    for (unsigned int i=0; i<this->matrix->GetOligsNum(); ++i) {
        cout << i << ": ";
        for(unsigned int j=0; j<this->deletedZerosInRows[i].size(); ++j)
            cout << this->deletedZerosInRows[i].at(j) << " ";
        cout << endl;
    }
}


// ----------------------------------------------------------- //


void HungarianAlgorithm::PrintDeletedZerosInCols() {
    cout << "Deleted zeros in columns" << endl;
    for (unsigned int i=0; i<this->matrix->GetOligsNum(); ++i) {
        cout << i << ": ";
        for(unsigned int j=0; j<this->deletedZerosInCols[i].size(); ++j)
            cout << this->deletedZerosInCols[i].at(j) << " ";
        cout << endl;
    }
}


// ----------------------------------------------------------- //


void HungarianAlgorithm::PrintChosenZerosInRows() {
    cout << "Chosen zeros in rows" << endl;
    for (unsigned int i=0; i<this->matrix->GetOligsNum(); ++i) {
        cout << i << ": ";
        if(this->choosenZerosInRows[i] != INF)
            cout << choosenZerosInRows[i];
        else
            cout << "-";
        cout << endl;
    }
}


// ----------------------------------------------------------- //


void HungarianAlgorithm::PrintChosenZerosInCols() {
    cout << "Chosen zeros in columns" << endl;
    for (unsigned int i=0; i<this->matrix->GetOligsNum(); ++i) {
        cout << i << ": ";
        if(this->choosenZerosInCols[i] != INF)
            cout << choosenZerosInCols[i];
        else
            cout << "-";
        cout << endl;
    }
}


// ----------------------------------------------------------- //


Matrix *HungarianAlgorithm::GetMatrix() {
    return this->matrix;
}


// ----------------------------------------------------------- //


Graph *HungarianAlgorithm::GetGraph() {
    return this->graph;
}


// ----------------------------------------------------------- //


unsigned int* HungarianAlgorithm::GetChoosenZerosInRows() {
    return this->choosenZerosInRows;
}

