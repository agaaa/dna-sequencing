#ifndef GRAPH
#define GRAPH

#include <iostream>
#include <vector>

#include "Def.h"

class Graph {

    private:
        Def *def;
        unsigned int oligsNum;
        std::vector<unsigned int> *graph; //lista następników

    public:
        Graph(unsigned int _oligsNum, Def *_def);
        ~Graph();

        void Create(unsigned int *checkedZerosInRows, unsigned int *checkedZerosInCols, std::vector<unsigned int> *deletedZeros);
        void Clear();
        bool FindPath(std::vector<unsigned int> &_path);

        void PrintGraph();
};

#endif
