#include "Algorithm.h"

using namespace std;

Algorithm::Algorithm(unsigned int _n, vector<string> data, string _dataFileName, string _resultFileName) { 
    this->n = _n;
    this->def = new Def();
    this->matrix = new Matrix(10, data, def);
    this->hungarianAlgorithm = new HungarianAlgorithm(matrix);

    this->oligs = this->matrix->GetOligs();
    this->graph = this->hungarianAlgorithm->GetGraph();
    this->choosenZeros = this->hungarianAlgorithm->GetChoosenZerosInRows();

    this->negative = 0;
    this->positive = this->matrix->GetOligsNum()-1;
    this->dataFileName = _dataFileName;
    this->resultFileName = _resultFileName;
}


// ----------------------------------------------------------- //


Algorithm::~Algorithm() {

}


// ----------------------------------------------------------- //


void Algorithm::Solve() {
    this->start = clock();

    unsigned int numberInResult, sum, first;
    this->hungarianAlgorithm->Solve();
    while (this->CheckIfSolved(numberInResult, sum) == false) {
        this->ImproveSolution();
    }
    first = this->FindSolution(numberInResult, sum);

    this->stop = clock();

    this->PrintResults(first, sum);
}


// ----------------------------------------------------------- //


bool Algorithm::CheckIfSolved(unsigned int &_num, unsigned int &_sumOfTheCycle) {
    bool *visited;
    visited = new bool [this->matrix->GetOligsNum()];
    for(unsigned int i=0; i<this->matrix->GetOligsNum(); ++i)
        visited[i] = false;

    unsigned int first, i, sum, max, distance, number, elem;
    bool continuation = true;

    first = i = _num = 0;
    do { 
        elem = 0;
        sum = 10; //olig length
        max = 0;
        number = 0;
        do {
            ++elem;
            distance = this->matrix->GetDistance(i, this->choosenZeros[i]);
            sum = sum + distance;

            if(distance > max)
                max = distance;
            visited[i] = true;
            i = this->choosenZeros[i];

            ++number;
        } while(i != first); 

        if((sum-max >= this->n) || elem == this->matrix->GetOligsNum()) {
            _sumOfTheCycle = sum;
            return true;
        }

        first = i = this->GetFalseIndex(visited);
        if(first == INF) {//all visited
            continuation = false;
        } else
            _num = first;

    } while(continuation == true);

    return false;
}


// ----------------------------------------------------------- //


unsigned int Algorithm::GetFalseIndex(bool *_tab) {

    for(unsigned int i=0; i<this->matrix->GetOligsNum(); ++i)
        if(_tab[i] == false)
            return i;
    return INF;
}


// ----------------------------------------------------------- //


unsigned int Algorithm::FindSolution(unsigned int _num, unsigned int &_sum) {

    this->distances.clear();
    this->FillVector(_num);
    
    unsigned int first, toDelete, length;
    toDelete = 0;
    do {
        ++toDelete;
        length = this->FindTheWorst(_num, toDelete, first);
    } while(_sum-length > this->n);
    _sum = _sum - length;

    this->DeleteTheWorst(first, toDelete);

    return first;
}


// ----------------------------------------------------------- //


//_num - number in cycle
void Algorithm::FillVector(unsigned int _num) {

    pair <unsigned int, unsigned int> element; 
    unsigned int distance, i;
    i = _num;
    do {
        distance = this->matrix->GetDistance(i, this->choosenZeros[i]);
        element = make_pair(i, distance);
        this->distances.push_back(element);
        i = this->choosenZeros[i]; //next element
    } while (i != _num);
}


// ----------------------------------------------------------- //


//_num - number in cycle
//elem - number of the worst elements 
unsigned int Algorithm::FindTheWorst(unsigned int _num, unsigned int elem, unsigned int &_first) {

    unsigned int first = _num, i, sum, distance, elements, toDelete, max, beginOfTheWorst;
    pair <unsigned int, unsigned int> element; 

    i = _num;
    sum = 0;
    elements = 0;
    while(elements < elem) {
        distance = this->distances.at(elements).second;
        sum = sum + distance;

        i = this->choosenZeros[i];
        ++elements;
    }   

    first = distances.at(elem-1).first; 
    toDelete = 0;
    max = sum;
    beginOfTheWorst = distances.at(toDelete).first;
    do {
        //next element
        distance = this->distances.at(elements % (this->distances.size())).second;

        //sum with next element without first element
        sum = sum + distance - distances.at(toDelete).second;
        if(sum > max) {
            max = sum;
            beginOfTheWorst = choosenZeros[distances.at(toDelete).first];
        }

        i = this->choosenZeros[i];
        ++toDelete;
        ++elements;

    } while (i != first);

    _first = beginOfTheWorst;
    return max;
}


// ----------------------------------------------------------- //


//_num - first number to delete
//elem - number of elemets
unsigned int Algorithm::DeleteTheWorst(unsigned int &_num, unsigned int elem) {

    unsigned int deleted = 0, i=_num, tmp;
    while (deleted < elem) {
        tmp = this->choosenZeros[i];
        this->choosenZeros[i] = INF;
        i = tmp;
        ++deleted;
    } 
    _num = i; //first element in result
    return 0;
}


// ----------------------------------------------------------- //


void Algorithm::PrintSolution(unsigned int _first) {

    unsigned int distMovement = this->FirstIndex(_first);
    unsigned int i, movement, position, j;
    string olig;

    cout << this->oligs.at(_first) << "|";
    i = this->choosenZeros[_first];
    j = 0;
    
    while (i != INF) {
        olig = this->oligs.at(i);
        movement = this->distances.at((distMovement+j)%this->distances.size()).second;

        this->negative = this->negative + movement - 1;
        --this->positive;
        
        position = 10 - movement;
        cout << olig.substr(position) << "|";
        
        i = this->choosenZeros[i];
        ++j;
    };
    cout << endl;

}


// ----------------------------------------------------------- //


//index of the _num in vector of the distances
unsigned int Algorithm::FirstIndex(unsigned int _first) {
    unsigned int i=0;
    while(distances.at(i).first != _first)
        ++i;
    return i;
}


// ----------------------------------------------------------- //


void Algorithm::ImproveSolution() {
    this->RemoveCycles();
    this->hungarianAlgorithm->~HungarianAlgorithm();
    this->hungarianAlgorithm = new HungarianAlgorithm(this->matrix);
    this->hungarianAlgorithm->Solve();
    this->choosenZeros = this->hungarianAlgorithm->GetChoosenZerosInRows();
}


// ----------------------------------------------------------- //


void Algorithm::RemoveCycles() {

    bool *visited;
    visited = new bool [this->matrix->GetOligsNum()];
    for(unsigned int i=0; i<this->matrix->GetOligsNum(); ++i)
        visited[i] = false;

    unsigned int first, i, theWorstId, theWorstValue, distance;
    bool continuation;

    first = i = 0;
    continuation = true;

    do { 
        //cycle
        theWorstId = i;
        theWorstValue = this->matrix->GetDistance(i, this->choosenZeros[i]);
        do {
            visited[i] = true;
            i = this->choosenZeros[i];

            distance = this->matrix->GetDistance(i, this->choosenZeros[i]);
            if(distance > theWorstValue) {
                theWorstId = i;
                theWorstValue = distance;
            }

        } while(i != first); 

        if(this->matrix->GetDistance(theWorstId, this->choosenZeros[theWorstId]) != 1)
            this->matrix->DisableConnection(theWorstId, this->choosenZeros[theWorstId]);

        first = i = this->GetFalseIndex(visited);
        if(first == INF) //all visited
            continuation = false;

    } while(continuation == true);

}


// ----------------------------------------------------------- //


void Algorithm::PrintResults(unsigned int _first, unsigned int _length) {

    this->PrintSolution(_first);
    cout << "Length: " << _length << endl;
    cout << "Positive: " << this->positive << endl;
    cout << "Negative: " << this->negative << endl << endl;

    ofstream ofs;
    ofs.open (this->resultFileName, std::ofstream::out | std::ofstream::app);
    ofs << this->dataFileName << "\t"
        << _length << "\t"
        << this->negative << "\t"
        << this->positive << "\t"
        << fixed << setprecision(2) << (double)(this->stop - this->start) / (double)CLOCKS_PER_SEC << endl;
    ofs.close();

}
