#include <iostream>
#include <vector>

#include "DNASequencing.h"

using namespace std;

int main(int argc, char* argv[]) {
    string dirName;
    unsigned int n;

    if(argc > 1) {
        stringstream ss;
        ss << argv[1];
        ss >> n;
        ss.clear();

        ss << argv[2];
        ss >> dirName;
        dirName = dirName;
        cout << dirName << endl;
    }
    else {
        cout << "Usage: ./DNA_SEQUENCING **sequence_length** **sequences_directory**" << endl;
        exit(1);
    } 

    DNASequencing seq = DNASequencing(dirName, n);
    seq.Run();

    return 0;
}
