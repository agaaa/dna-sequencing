#ifndef MATRIX
#define MATRIX

#include <iostream>
#include <vector>

#include "Def.h"

class Matrix {

    private:
        Def *def;
        unsigned int wordLength;
        unsigned int oligsNum;
        unsigned int **data;
        unsigned int **matrix;
        std::vector<std::string> oligs;

    public:
        Matrix(unsigned int _wordLength, std::vector<std::string> _oligs, Def *_def);
        ~Matrix();

        unsigned int CountDistance(std::string _olig1, std::string _olig2); //distance between two oligs
        void Create(std::vector<std::string> oligs); 

        unsigned int MinInRow(unsigned int _rowNum);
        unsigned int MinInCol(unsigned int _colNum);
        
        void SubtractFromRow(unsigned int _rowNum, unsigned int _value);
        void SubtractFromCol(unsigned int _colNum, unsigned int _value);
        
        void GetZerosInRow(unsigned int _rowNum, std::vector<unsigned int> & _zerosIdx);
        void GetZerosInCol(unsigned int _colNum, std::vector<unsigned int> & _zerosIdx);
        
        unsigned int GetOligsNum();
        std::vector<std::string> GetOligs();
        unsigned int GetElement(unsigned int x, unsigned int y); //edited matrix
        unsigned int GetDistance(unsigned int x, unsigned int y); //initial data

        void SubtractFromUnlinedRowsAndAddToLinedCols(unsigned int _value,
                std::vector<unsigned int> _unlinedRows, std::vector<unsigned int> _linedCols);
        void DisableConnection(unsigned int x, unsigned int y);

        Def* GetDef();

        void PrintMatrix();
        void PrintInitialMatrix();
};

#endif
