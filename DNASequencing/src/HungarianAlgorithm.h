#ifndef HUNGARIAN
#define HUNGARIAN

#include <iostream>

#include "Matrix.h"
#include "Graph.h"
#include "Def.h"

class HungarianAlgorithm {

    private:
        Matrix *matrix;
        Graph *graph;
        Def *def;

        std::vector<unsigned int> *zerosInRows;
        std::vector<unsigned int> *zerosInCols;

        std::vector<unsigned int> *deletedZerosInRows;
        std::vector<unsigned int> *deletedZerosInCols;
        
        unsigned int *choosenZerosInRows;
        unsigned int *choosenZerosInCols;
        
        std::vector<unsigned int> choosenRows;
        std::vector<unsigned int> choosenCols;
        
        std::vector<unsigned int> linesRows;
        std::vector<unsigned int> linesCols;

    public:
        HungarianAlgorithm(Matrix *_matrix);
        ~HungarianAlgorithm();

        void Solve();

        void SubtractMinFromRows();
        void SubtractMinFromCols();

        void MainLoop();

        void InitVariables();
        void FillVariables();
        void UpdateVariables();
        void ClearVariables();

        void CheckTheOnlyOneZerosInRows(bool &modified);
        void CheckTheOnlyOneZerosInCols(bool &modified);
        void CheckNWZero();

        void ClearFromRows(unsigned int _colNum);
        void ClearFromCols(unsigned int _rowNum);

        bool ZerosInRowsAllocated();
        bool UncheckedZerosExist();

        void CheckingRowsAndCols();
        void CheckRowsWithoutCheckedZeros();
        void CheckColWithZero(bool &modified);
        void CheckRowWithCheckedZerosInCheckCols(bool &modified);

        void CoverRowsAndCols();
        void CoverRows();
        void CoverCols();
        unsigned int GetCheckedZerosNumber();
        unsigned int MinUnlinedElement();
        void SolveGraph();

        void PrintZerosInRows();
        void PrintZerosInCols();
        void PrintDeletedZerosInRows();
        void PrintDeletedZerosInCols();
        void PrintChosenZerosInRows();
        void PrintChosenZerosInCols();

        Matrix* GetMatrix();
        Graph* GetGraph();
        unsigned int* GetChoosenZerosInRows();
};

#endif
