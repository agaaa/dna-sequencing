#include "Matrix.h"

using namespace std;

Matrix::Matrix(unsigned int _wordLength, vector<string> _oligs, Def *_def) {
    this->wordLength = _wordLength;
    this->oligsNum = _oligs.size();
    this->Create(_oligs);
    this->oligs = _oligs;
    this->def = _def;
}


// ----------------------------------------------------------- //


Matrix::~Matrix() {
    for (unsigned int i=0; i<this->oligsNum; i++) {
        delete [] matrix[i];
        delete [] data[i];
    }
    delete [] matrix;
    delete [] data;
}


// ----------------------------------------------------------- //


//distance between two oligonucleotides [1; wordLength]
unsigned int Matrix::CountDistance(string _olig1, string _olig2) {
    unsigned int distance = 1;

    bool found = false;
    unsigned int i=0, j;

    while (found == false) {
        j = i;
        while ( (_olig1[j+distance] == _olig2[j]) && (j < (this->wordLength)) ) {
            ++j;
        }

        if ( j == ((this->wordLength) - distance) )
            found = true;
        else
            ++distance;
    }

    return distance;
}


// ----------------------------------------------------------- //


// creating a matrix of distances of all words
void Matrix::Create(vector<string> _oligs) {

    unsigned int distance;

    this->matrix = new unsigned int*[this->oligsNum];
    this->data = new unsigned int*[this->oligsNum];
    for (unsigned int i=0; i<this->oligsNum; ++i) {
        this->matrix[i] = new unsigned int[this->oligsNum];
        this->data[i] = new unsigned int[this->oligsNum];
    }

    for(unsigned int i=0; i<this->oligsNum; ++i) {
        for(unsigned int j=0; j<this->oligsNum; ++j) {
            if (i == j) {
                this->matrix[i][j] = INF;
                this->data[i][j] = INF;
            }
            else {
                distance = this->CountDistance(_oligs.at(i), _oligs.at(j));
                this->matrix[i][j] = distance;
                this->data[i][j] = distance;
            }
        }
    }
}


// ----------------------------------------------------------- //


//value of minimum element in row
unsigned int Matrix::MinInRow(unsigned int _rowNum) {
    unsigned int min = this->matrix[_rowNum][0];
    for(unsigned int i=1; i<this->oligsNum; ++i) {
        if(this->matrix[_rowNum][i] < min)
            min = this->matrix[_rowNum][i];
    }
    return min;
}


// ----------------------------------------------------------- //


//value of minimum element in column
unsigned int Matrix::MinInCol(unsigned int _colNum) {
    unsigned int min;
    min = this->matrix[0][_colNum];
    for(unsigned int i=1; i<this->oligsNum; ++i) {
        if(this->matrix[i][_colNum]< min)
            min = this->matrix[i][_colNum];
    }
    return min;
}


// ----------------------------------------------------------- //


//substraction form each element in row
void Matrix::SubtractFromRow(unsigned int _rowNum, unsigned int _value) {
    for(unsigned int i=0; i<this->oligsNum; ++i)
        if(this->matrix[_rowNum][i] != INF)
            this->matrix[_rowNum][i] = this->matrix[_rowNum][i] - _value;
}


// ----------------------------------------------------------- //


//subtraction from each element in column
void Matrix::SubtractFromCol(unsigned int _colNum, unsigned int _value) {
    for(unsigned int i=0; i<this->oligsNum; ++i)
        if(this->matrix[i][_colNum] != INF)
            this->matrix[i][_colNum] = this->matrix[i][_colNum] - _value;
}


// ----------------------------------------------------------- //


void Matrix::GetZerosInRow(unsigned int _rowNum, vector<unsigned int> & _zerosIdx) {
    for(unsigned int i=0; i<this->oligsNum; ++i)
        if(this->matrix[_rowNum][i] == 0)
            _zerosIdx.push_back(i);
}


// ----------------------------------------------------------- //


void Matrix::GetZerosInCol(unsigned int _colNum, vector<unsigned int> & _zerosIdx) {
    for(unsigned int i=0; i<this->oligsNum; ++i)
        if(this->matrix[i][_colNum] == 0)
            _zerosIdx.push_back(i);
}


// ----------------------------------------------------------- //


unsigned int Matrix::GetOligsNum() {
    return this->oligsNum;
}


// ----------------------------------------------------------- //


vector<string> Matrix::GetOligs() {
    return this->oligs;
}


// ----------------------------------------------------------- //


//a value of element in modified matrix
unsigned int Matrix::GetElement(unsigned int x, unsigned int y) {
    return this->matrix[x][y];
}


// ----------------------------------------------------------- //


//a value of element in initial matrix (distance between oligs)
unsigned int Matrix::GetDistance(unsigned int x, unsigned int y) {
    return this->data[x][y];
}


// ----------------------------------------------------------- //


void Matrix::SubtractFromUnlinedRowsAndAddToLinedCols(unsigned int _value,
        vector<unsigned int> _choosenRows, vector<unsigned int> _choosenCols) {

    bool choosenRow, choosenCol;
    for(unsigned int rowNum=0; rowNum<this->oligsNum; ++rowNum) {

        if(this->def->Contains(rowNum, _choosenRows) == true)
            choosenRow = true;
        else
            choosenRow = false;

        for(unsigned int colNum=0; colNum<this->oligsNum; ++colNum) {
            if(this->matrix[rowNum][colNum] != INF) {

                if(this->def->Contains(colNum, _choosenCols) == true)
                    choosenCol = true;
                else
                    choosenCol = false;

                if(choosenRow == true && choosenCol == false)  //minus
                    this->matrix[rowNum][colNum] = this->matrix[rowNum][colNum] - _value;
                else if(choosenRow == false && choosenCol == true)  //plus
                    this->matrix[rowNum][colNum] = this->matrix[rowNum][colNum] + _value;
            }
        }
    }

}


// ----------------------------------------------------------- //


void Matrix::DisableConnection(unsigned int x, unsigned int y) {
    this->matrix[x][y] = INF;
}


// ----------------------------------------------------------- //


Def* Matrix::GetDef() {
    return this->def;
}

// ----------------------------------------------------------- //


void Matrix::PrintMatrix() {
    for(unsigned int i=0; i<this->oligsNum; ++i) {
        for(unsigned int j=0; j<this->oligsNum; ++j) {
            if (i == j)
                cout << "X  ";
            else {
                if (this->matrix[i][j] < 10)
                    cout << " ";
                cout << this->matrix[i][j] << " ";
            }
        }
        cout << endl;
    }
}


// ----------------------------------------------------------- //


void Matrix::PrintInitialMatrix() {
    for(unsigned int i=0; i<this->oligsNum; ++i) {
        for(unsigned int j=0; j<this->oligsNum; ++j) {
            if (i == j)
                cout << "X  ";
            else {
                if (this->data[i][j] < 10)
                    cout << " ";
                cout << this->data[i][j] << " ";
            }
        }
        cout << endl;
    }
}

