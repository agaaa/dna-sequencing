#include "Graph.h"

using namespace std;

Graph::Graph(unsigned int _oligsNum, Def *_def) {
    this->oligsNum = _oligsNum;
    this->def = _def;
    this->graph = new vector<unsigned int> [2*_oligsNum + 2];
    //0 - oligsNum-1 -> w
    //oligsNum - 2*oligsNum-1 -> z
    //2*oligsNum -> start
    //2*oligsNum+1 -> stop
}


// ----------------------------------------------------------- //


Graph::~Graph() {
    this->Clear();
    delete [] graph;
}


// ----------------------------------------------------------- //


void Graph::Create(unsigned int *checkedZerosInRows, unsigned int *checkedZerosInCols, vector<unsigned int> *deletedZeros) {

    unsigned int rowNum, colNum;
    vector<unsigned int> choosenCols;

    for(rowNum=0; rowNum<this->oligsNum; ++rowNum) {

        //Dla każdego naznaczonego zera o współrzędnych (i,j) utwórz łuk zj → wi
        if(checkedZerosInRows[rowNum] != INF) {
            colNum = checkedZerosInRows[rowNum];
            choosenCols.push_back(colNum);
            this->graph[colNum+this->oligsNum].push_back(rowNum);
        }
        //dla wierszy i bez przydziału - łuk s → wi;
        else {
            this->graph[2*(this->oligsNum)].push_back(rowNum);
        }
    }

    //dla zer skreślonych o współrzędnych (i,j) - łuk wi → zj;
    //cout << "Skreślone zera: " << endl;
    for(rowNum=0; rowNum<this->oligsNum; ++rowNum) {
        for(unsigned int i=0; i<deletedZeros[rowNum].size(); ++i) {
            colNum = deletedZeros[rowNum].at(i);
            //cout << "\t- (" << rowNum << ", " << colNum << ")" << endl;
            this->graph[rowNum].push_back(colNum+this->oligsNum);
        }
    }

    //dla kolumn j bez przydziału – łuk zj → t
    for(colNum=0; colNum<this->oligsNum; ++colNum) {
        if(this->def->Contains(colNum, choosenCols) == false)
            this->graph[colNum+this->oligsNum].push_back(2*(this->oligsNum)+1);
    }
}


// ----------------------------------------------------------- //


void Graph::Clear() {
    for(unsigned int i=0; i<2*this->oligsNum+2; ++i)
        graph[i].clear();
}


// ----------------------------------------------------------- //


bool Graph::FindPath(vector<unsigned int> &_path) {

    unsigned int tmp,
                 id = 2*this->oligsNum; //start
    _path.push_back(id);

    while(id != 2*this->oligsNum+1) { //stop

        //dalej
        if (this->graph[id].size() > 0) { //has adjacents
            tmp = this->graph[id].back(); //next element
            this->graph[id].pop_back(); //remove connection
            id = tmp;
            _path.push_back(id);
        }

        //cofnij
        else {
            _path.pop_back(); //usunięcie aktualnego (pustego) elementu
            if (_path.size() == 0)
                return false;
            id = _path.back();
        }
    }
    _path.erase(_path.begin()); //start

    return true;
}


// ----------------------------------------------------------- //


void Graph::PrintGraph() {
    for(unsigned int i=0; i<2*this->oligsNum+2; ++i) {
        if(i<this->oligsNum)
            cout << "row " << i << " : ";
        else if(i<2*this->oligsNum)
            cout << "col " << i-this->oligsNum << " : ";
        else if(i==2*this->oligsNum)
            cout << "start : ";
        else
            cout << "stop : ";

        unsigned int value;
        unsigned int size = this->graph[i].size();
        for(unsigned int j=0; j<size; ++j) {
            value = this->graph[i].at(j);
            if(value<this->oligsNum)
                cout << "row " << value;
            else if(value<2*this->oligsNum)
                cout << "col " << value-this->oligsNum;
            else if(value==2*this->oligsNum)
                cout << "start";
            else
                cout << "stop";
            if(j != size-1)
                cout << ", ";
        }
        cout << endl;
    }
}


