#include "Def.h"

using namespace std;

Def::Def() {

};


// ----------------------------------------------------------- //


Def::~Def() {

};


// ----------------------------------------------------------- //


bool Def::Contains(unsigned int _value, vector<unsigned int> _vec) {
    unsigned int i=0,
                 size = _vec.size();
    while(i < size) {
        if( _vec.at(i) == _value) {
            return true;
        }
        ++i;
    }   
    return false;
}


// ----------------------------------------------------------- //


bool Def::Remove(unsigned int _value, vector<unsigned int> &_vec) {
    unsigned int i=0;
    while(i<_vec.size()) {
        if( _vec.at(i) == _value) {
            _vec.erase(_vec.begin()+i);
            return true;

        }
        ++i;
    }
    return false;
}


