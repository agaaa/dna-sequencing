# DNA Sequencing - README #

## Description ##
The aim of this project is DNA chain reconstruction.

Given:

* m - number of oligonucleotides

* l - length of oligonucleotides

* n - original sequence length

The algorithm is based on Hungarian algorithm.

## Building ##
mkdir bin
cd bin
cmake ..
make

## Running ##
./src/DNA_SEQUENCING **original_DNA_length** **data_directory**


Example: 

./src/DNA_SEQUENCING 220 ../data/negative/209/